#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	uint64_t lastBodyId = bodyId;

	//ofSetFrameRate(45);
	ofSetVerticalSync(false);

	receiver.setup(PORT);
	currentMsgString = 0;

	sea.load("assets/sea/MarAzulok.mp4");
	sea.play();

	platform.load("assets/img/3.png");
	columns.load("assets/img/4.png");
	birds.load("assets/img/7.png");
	snow.load("assets/img/6.png");

	fish1.load("assets/img/8_crop.png");
	fish1.setup(1000,350);

	fish2.load("assets/img/9_crop.png");
	fish2.setup(1000, 150);

	fish3.load("assets/img/10_crop.png");
	fish3.setup(1000, 150);

	bird2.load("assets/img/11_crop.png");
	bird2.setup(800, 250);
}

//--------------------------------------------------------------
void ofApp::update(){

	sea.update();
	fish1.moveHorizontal(-80, 1400, -200, ofGetMouseX(), 1);

	//if (fish1.detectOverlap(ofGetMouseX())) {
	//	ofLogNotice() << "Estan tocando";
	//}

	fish2.moveHorizontal(-30, 1400, -200, ofGetMouseX(), 1);
	fish3.moveHorizontal(20, 1400, -200, ofGetMouseX(), 1);

	bird2.moveSin(800, 850);


	oscReceiver();
}

//--------------------------------------------------------------
void ofApp::draw(){
	sea.draw(0, 0);
	platform.draw(0, 0);
	columns.draw(0, 0);
	birds.draw(0, 0);
	snow.draw(0, 0);

	fish1.draw();
	fish2.draw();
	fish3.draw();

	bird2.draw();
}

//--------------------------------------------------------------
void ofApp::oscReceiver(){
	// hide old messages
	for (int i = 0; i < NUM_MSG_STRINGS; i++) {
		if (timers[i] < ofGetElapsedTimef()) {
			msgStrings[i] = "";
		}
	}

	// check for waiting messages
	while (receiver.hasWaitingMessages()) {
		// get the next message
		ofxOscMessage m;
		receiver.getNextMessage(m);

		// check for mouse moved message
		if (m.getAddress() == "/kinect/body/id") {
			bodyId = m.getArgAsInt64(0);
		}
		else if (m.getAddress() == "/kinect/hands/right") {
			handRight.x = m.getArgAsFloat(0);
			handRight.y = m.getArgAsFloat(1);
			handRight.z = m.getArgAsFloat(2);
		}
		else if (m.getAddress() == "/kinect/hands/left") {
			handLeft.x = m.getArgAsFloat(0);
			handLeft.y = m.getArgAsFloat(1);
			handLeft.z = m.getArgAsFloat(2);
		}
		//else if (m.getAddress() == "/kinect/head") {
		//	// both the arguments are float's
		//	head.x = m.getArgAsFloat(0);
		//	head.y = m.getArgAsFloat(1);
		//	head.z = m.getArgAsFloat(2);
		//}
		else {
			// unrecognized message: display on the bottom of the screen
			string msg_string;
			msg_string = m.getAddress();
			msg_string += ":";
			for (int i = 0; i < m.getNumArgs(); i++) {
				// get the argument type
				msg_string += " ";
				msg_string += m.getArgTypeName(i);
				msg_string += ":";
				// display the argument - make sure we get the right type
				if (m.getArgType(i) == OFXOSC_TYPE_INT32) {
					msg_string += ofToString(m.getArgAsInt32(i));
				}
				else if (m.getArgType(i) == OFXOSC_TYPE_FLOAT) {
					msg_string += ofToString(m.getArgAsFloat(i));
				}
				else if (m.getArgType(i) == OFXOSC_TYPE_STRING) {
					msg_string += m.getArgAsString(i);
				}
				else {
					msg_string += "unknown";
				}
			}
			// add to the list of strings to display
			msgStrings[currentMsgString] = msg_string;
			timers[currentMsgString] = ofGetElapsedTimef() + 5.0f;
			currentMsgString = (currentMsgString + 1) % NUM_MSG_STRINGS;
			// clear the next line
			msgStrings[currentMsgString] = "";
		}
	}

	ofLogNotice() << "DERECHA" << ofToString(handRight.x);
}
//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//void ofApp::detectCollisions()
//{
//	if (ofGetMouseX() < fish1.getMaxX() && ofGetMouseX() > fish1.getMinX()) {
//		ofLogNotice() << "Estan tocando";
//	}
//	else {
//		ofLogNotice() << "NO Estan tocando";
//	}
//
//
//}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
