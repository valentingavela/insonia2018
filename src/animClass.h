#pragma once
#include "ofMain.h"

class animClass
{
public:
	void load(string _imgname);
	void draw();
	void moveHorizontal(int speed, int _maxX, int _minX, int _otherObjMinX, int _maxTimes);
	void moveSin(int _maX, int _minX);
	void moveNoise();
	void setup(int _xPos, int _yPos);

	string imgname;
	int xPos;
	int initXPos;
	int yPos;
	int actualTurn;
	

	ofImage image;

	int getMaxX();
	int getMaxY();

	int getMinX();
	int getMinY();

	ofRectangle rectangle;

	bool detectOverlap(int _otherObjMinX);
};

