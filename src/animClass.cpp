#include "animClass.h"

void animClass::load(string _imgname)
{
	imgname = _imgname;
	image.load(imgname);
	image.setAnchorPoint(image.getWidth() / 2, image.getHeight() / 2);	
}

void animClass::setup(int _xPos, int _yPos)
{
	xPos = _xPos;
	initXPos = xPos;
	yPos = _yPos;

	actualTurn = 0; // animate functions
}

bool animClass::detectOverlap(int _otherObjMinX)
{
	bool touching;
	if (_otherObjMinX < rectangle.getMaxX() && _otherObjMinX > rectangle.getMinX()) {
		ofLogNotice() << "Esta tocando";
		touching = true;
	}
	else {
		ofLogNotice() << "NO esta tocando";
		touching = false;
	}

	return touching;
}


void animClass::draw()
{
	image.draw(xPos, yPos);
	rectangle.set(xPos, xPos, image.getHeight(), image.getWidth());
}

void animClass::moveHorizontal(int speed, int _maxX, int _minX, int _otherObjMinX, int _maxTurns)
{
	bool overlap = detectOverlap(_otherObjMinX);

	if (overlap && actualTurn > _maxTurns) {
		actualTurn = 0;
	}
	if (_maxTurns >= actualTurn) {
		if (speed > 0) {
			if (xPos > 1400) {
				xPos = -200;
				actualTurn +=1;
			}
		}
		else
		{
			if (xPos < -200) {
				xPos = 1300;
				actualTurn += 1;
			}
		}
		xPos += ofGetLastFrameTime() * speed;
	}else if(initXPos-5 < xPos && xPos > initXPos + 5){
		xPos += ofGetLastFrameTime() * speed;
		ofLogNotice() << "VOLVER A SU LUGAR";
	}
	else {
		ofLogNotice() << "VOLVIO A SU LUGAR";
	}

	ofLogNotice() << "TURN" << ofToString(actualTurn);

}

void animClass::moveSin(int _maX, int _minX)
{
	xPos = ofMap(sin(ofGetElapsedTimef()), -1, 1, _maX, _minX);
}

void animClass::moveNoise()
{
	float noise = ofNoise(0.0 / 100.0);
	xPos = ofMap(noise, 0, 1, 0, 1000);
}

/// Getters
int animClass::getMaxX()
{
	return rectangle.getMaxX();
}

int animClass::getMaxY()
{
	return rectangle.getMaxY();
}

int animClass::getMinX()
{
	return rectangle.getMinX();
}

int animClass::getMinY()
{
	return rectangle.getMinY();
}
