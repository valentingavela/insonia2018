#pragma once

#include "ofMain.h"
#include "animClass.h"
#include "ofxOsc.h"

#define PORT 9001
#define NUM_MSG_STRINGS 20

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		//OSC
		void oscReceiver();
		ofxOscReceiver receiver;

		int currentMsgString;
		string msgStrings[NUM_MSG_STRINGS];
		float timers[NUM_MSG_STRINGS];

		float mouseXf = 0;
		float mouseYf = 0;
		int mouseButtonInt = 0;
		string mouseButtonState = "";
		ofImage receivedImage;
		//Kinect
		ofVec3f handLeft;
		ofVec3f handRight;
		uint64_t bodyId;

		//Environment
		ofVideoPlayer sea;
		
		ofImage platform;
		ofImage columns;
		ofImage birds;
		ofImage snow;

		animClass fish1;
		animClass fish2;
		animClass fish3;
		animClass bird2;




};
